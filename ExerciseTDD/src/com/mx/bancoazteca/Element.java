package com.mx.bancoazteca;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Element {

	// Cyclomatic Complexity 4
	public String toxicityClassification(double level) {
		String toxicity = "";
		if (level <= 3.8)
			toxicity = "VERDE";
		if (level >= 3.81 && level <= 8.25)
			toxicity = "AZUL";
		if (level >= 8.26 && level <= 15)
			toxicity = "AMARILLO";
		if (level > 15)
			toxicity = "ROJO";
		return toxicity;
	}

	// Cyclomatic Complexity 1
	public String toxicityLevelCalculation(Integer ph, String plasmaLevel, String concentration, String acidity,
			String type, String composition) {
		
		DecimalFormat df2 = new DecimalFormat("#.##");
		df2.setRoundingMode(RoundingMode.DOWN);
		Double toxicityLevel = 0.0;
		Integer plasmaLevelNumber = getNumberPlasmaLevel(plasmaLevel);
		Integer concentrationNumber = getNumberConcentration(concentration);
		Integer acidityNumber = getNumberAcidity(acidity);
		Integer typeNumber = getNumberType(type);
		Integer compositionNumber = getNumberComposition(composition);
				
		double phMasplasmaLevel = (double) (ph+plasmaLevelNumber);		
		double concentrationPorAcidity = (double) (concentrationNumber*acidityNumber);
		double typeMasComposition = (double)(typeNumber+compositionNumber);				
		toxicityLevel = (phMasplasmaLevel + concentrationPorAcidity)/typeMasComposition;
		System.out.println("toxicityLevel = " + toxicityLevel);
		return df2.format(toxicityLevel);
	}
	
	// Cyclomatic Complexity 1
	public Integer getNumberPlasmaLevel(String plasmaLevel) {
		Integer valueInteger = 8;
		if (plasmaLevel == "20-30") valueInteger = 6;
		return valueInteger;
	}
	
	// Cyclomatic Complexity 2
	public Integer getNumberConcentration(String concentration) {
		Integer valueInteger = 20;
		if (concentration == "BAJA") valueInteger = 10;
		if (concentration == "MEDIA") valueInteger = 15;
		return valueInteger;
	}
	
	// Cyclomatic Complexity 1
	public Integer getNumberAcidity(String acidity) {
		Integer valueInteger = 2;
		if (acidity == "ALCALINO") valueInteger = 4;		
		return valueInteger;
	}
	
	// Cyclomatic Complexity 1
	public Integer getNumberType(String ype) {
		Integer valueInteger = 5;
		if (ype == "HOMEOPATICO") valueInteger = 3;		
		return valueInteger;
	}
	
	// Cyclomatic Complexity 1
	public Integer getNumberComposition(String composition) {
		Integer valueInteger = 2;
		if (composition == "EXCIPIENTE") valueInteger = 4;		
		return valueInteger;
	}

}
