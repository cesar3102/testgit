package com.mx.bancoazteca.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.mx.bancoazteca.Element;

class TestElement {

	@Test
	void testToxicityClassificationGreen() {
		Element element = new Element();
		assertEquals("VERDE", element.toxicityClassification(0));
		assertEquals("VERDE", element.toxicityClassification(3.8));
	}
	
	@Test
	void testToxicityClassificationBlue() {
		Element element = new Element();
		assertEquals("AZUL", element.toxicityClassification(3.81));
		assertEquals("AZUL", element.toxicityClassification(8.25));
	}
	@Test
	void testToxicityClassificationYellow() {
		Element element = new Element();
		assertEquals("AMARILLO", element.toxicityClassification(8.26));
		assertEquals("AMARILLO", element.toxicityClassification(15));
		
		
	}
	@Test
	void testToxicityClassificationRed() {
		Element element = new Element();
		assertEquals("ROJO", element.toxicityClassification(15.1));
	}

	
	@Test
	void testToxicityLevelCalculation() {
		Element element = new Element();
		assertEquals("7.85", element.toxicityLevelCalculation(7,"10-20","ALTA","BASICO","MTP","ACTIVO"));
		assertEquals("7.57", element.toxicityLevelCalculation(7,"20-30","BAJA","ALCALINO","HOMEOPATICO","EXCIPIENTE"));
		assertEquals("6.42", element.toxicityLevelCalculation(7,"10-20","MEDIA","BASICO","MTP","ACTIVO"));
		
		assertEquals("8.4", element.toxicityLevelCalculation(6,"20-30","MEDIA","BASICO","HOMEOPATICO","ACTIVO"));
		assertEquals("13.42", element.toxicityLevelCalculation(6,"10-20","ALTA","ALCALINO","MTP","ACTIVO"));
		assertEquals("3.77", element.toxicityLevelCalculation(6,"10-20","BAJA","BASICO","MTP","EXCIPIENTE"));
		
		assertEquals("3.88", element.toxicityLevelCalculation(7,"10-20","BAJA","BASICO","MTP","EXCIPIENTE"));
		assertEquals("10.71", element.toxicityLevelCalculation(7,"10-20","MEDIA","ALCALINO","MTP","ACTIVO"));
		assertEquals("10.6", element.toxicityLevelCalculation(7,"20-30","ALTA","BASICO","HOMEOPATICO","ACTIVO"));
		
		
	}
	
	
}
